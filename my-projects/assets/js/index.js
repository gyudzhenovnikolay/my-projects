let burgerBtn = document.querySelector(".burger-btn");
let burgerMenu = document.querySelector(".burger-menu");
let advertising = document.querySelector(".advertising");
let closeBtn = document.querySelector(".close-btn");
burgerBtn.addEventListener("click", () => {
    if (!burgerMenu.classList.contains("open")) {
        burgerMenu.classList.add("open");
        burgerBtn.classList.add("open-burger");
        return;
    }
    if(burgerMenu.classList.contains("open")) {
        burgerMenu.classList.remove("open");
        burgerBtn.classList.remove("open-burger");
    }
});
//////////////////////////////////////////////////
let contact = document.querySelector(".info");
let contactBtn = document.querySelector("#contact")
contactBtn.addEventListener("click", openInfo);
function openInfo() {
    advertising.classList.add("open");
}
closeBtn.addEventListener("click", adverClose);
function adverClose() {
    advertising.classList.remove("open");
};
////////////////////////////////////////////////
let leftArrow = document.querySelector("#leftArrow");
let rightArrow = document.querySelector("#rightArrow");
let infoHeader = document.querySelector("#info h2");
let infoParagraph = document.querySelector("#info p");
let count = 1;
rightArrow.addEventListener("click", textForward);
leftArrow.addEventListener("click", textBack);
function textForward() {
    count ++;
    if(count === 1) {
        infoHeader.innerHTML = `Nikolay Svilenov Gyudzhenov`
        infoParagraph.innerHTML =`I am 36 years old born in Varna, Bulgaria on 22 of  January 1986.
        I studied RKMM in VMG - Varna and graduated secondary education in 2014.`
    } if (count === 2) {
        infoHeader.innerHTML = `Info`
        infoParagraph.innerHTML =`In the past I worked as a sales representative. Then i moved to Uk and worked as a curtain wall fixer for a short period of time.
        Now I work in S&N Plast as windows and door maker but I want to change my job and I started to learn HTML and CSS Fundamentals on my own. After 8 months I started FrontEnd Developer course in Advance Academy.`
    } if (count === 3) {
        infoHeader.innerHTML = `Hobbies`
        infoParagraph.innerHTML =`I like extreme sports and playing video games.
        My favourite games are Myst, Aura, Black Mirrors and more…`
    } if (count === 4) {
        infoHeader.innerHTML = `Language`
        infoParagraph.innerHTML =`English : working level`
        return count = 0;
    } 
};
function textBack() {
    count --;
    if(count === -1) {
        infoHeader.innerHTML = `Nikolay Svilenov Gyudzhenov`
        infoParagraph.innerHTML =`I am 36 years old born in Varna, Bulgaria on 22 of  January 1986.
        I studied RKMM in VMG - Varna and graduated secondary education in 2014.`
    } if (count === -2) {
        infoHeader.innerHTML = `Info`
        infoParagraph.innerHTML =`In the past I worked as a sales representative. Then i moved to Uk and worked as a curtain wall fixer for a short period of time.
        Now I work in S&N Plast as windows and door maker but I want to change my job and I started to learn HTML and CSS Fundamentals on my own. After 8 months I started FrontEnd Developer course in Advance Academy.`
    } if (count === -3) {
        infoHeader.innerHTML = `Hobbies`
        infoParagraph.innerHTML =`I like extreme sports and playing video games.
        My favourite games are Myst, Aura, Black Mirrors and more…`
    } if (count === -4) {
        infoHeader.innerHTML = `Language`
        infoParagraph.innerHTML =`English : working level`
        return count = 0;
    } 
};